import json
from collections import deque
import resolvers


def resolve(target_data):
    if len(target_data['targets_resolve_url']) == 0:
        return
    resolver = getattr(resolvers, target_data['resolve_class'])(target_data['targets'],
                                                                target_data['targets_resolve_url'])
    resolver.resolve()


def main():
    idx = json.load(open('./data/updated_index.json', 'r'))
    q = deque()
    q.append(idx)
    while len(q) > 0:
        n = q.pop()
        for target_name, target_data in n.items():
            if len(target_data['targets']) > 0:
                q.appendleft(target_data['targets'])
            else:
                resolve(target_data)

    json.dump(idx, open('./data/updated_index.json', 'w'), indent=True, ensure_ascii=False)

if __name__ == '__main__':
    main()
