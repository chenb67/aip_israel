import requests
from lxml import html


class BaseResolver(object):
    def __init__(self, targets_dict, url):
        self._targets_dict = targets_dict
        self._url = url

    def _request(self):
        c = requests.get(self._url)
        assert c.ok
        self._req_content = c.content

    def _parse_html(self):
        self._tree = html.fromstring(self._req_content)

    @staticmethod
    def node_template(url):
        return {
            'targets_resolve_url': url,
            'targets': {},
            'resolve_class': 'PassResolver'
        }

    def resolve(self):
        self._request()
        self._parse_html()


class SimpleFilesResolver(BaseResolver):
    def resolve(self):
        super(SimpleFilesResolver, self).resolve()
        links = self._tree.xpath('//div[@itemprop="articleBody"]//a')
        for e in links:
            self._targets_dict[e.text.strip()] = self.node_template(e.xpath('@href')[0])


class SimpleFoldersResolver(BaseResolver):
    def resolve(self):
        pass
        #super(SimpleFoldersResolver, self).resolve()


class PassResolver(BaseResolver):
    def resolve(self):
        pass
